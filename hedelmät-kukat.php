<!DOCTYPE html>

<?php 
	session_start();
	
?>

<html>

<?php include("head.php"); ?>

<header>
</header>

<body >

<!--Sivuvalikko -->

<div id="leftmenu">
	<div class="menubutton" id="passibutton"><p><a href="ravinnetutka2.php">Valonsaanti</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="Kasvualusta.php">Kasvualusta</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="Lehdet.php">Lehdet</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="Kasvu.php">Kasvu</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="runko.php">Runko</a></p></div>
	<div class="menubutton" id="actibutton"><p><a href="hedelmät-kukat.php">Hedelmät</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="lämpötila.php">Lämpötila</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="ratkaise.php">Ratkaise</a></p></div>
</div>

<!--Keskinäkymä-->

<div id="center-view">

	<div id="back"><a href="runko.php">Takaisin</a></div>
	<div id="next"><a href="lämpötila.php">Seuraava</a></div>

	<div id="options">

		<!-- PHP script valintojen tallettamista varten -->

		<?php 
			if (isset($_POST["kukat"])) {
				$_SESSION["kukat"] = $_POST["kukat"];	
			} else {}
		?>
		
		<!-- Valinnat -->

		<h3>Millaisia ovat hedelmät tai kukat?</h3>
		<form name="kukat" action="" method="POST" target="">
			<input type="checkbox" name="kukat[]" value="small"<?php if(@in_array ("small", $_SESSION["kukat"])) {echo "checked";} else {} ?>/>Pieniä<br>
			<input type="checkbox" name="kukat[]" value="big" <?php if(@in_array ("big", $_SESSION["kukat"])) {echo "checked";} else {} ?>/>Ylisuuria<br>
			<input type="checkbox" name="kukat[]" value="deform" <?php if(@in_array ("deform", $_SESSION["kukat"])) {echo "checked";} else {} ?>/>Epämuodostuneita<br>
			<input type="checkbox" name="kukat[]" value="mold" <?php if(@in_array ("mold", $_SESSION["kukat"])) {echo "checked";} else {} ?>/>Likaisia/homeisia<br>
			<input type="submit" name="submit" value="Tallenna"/>

		</form>

		<!-- Notifier scripti  -->
	
		<?php if (isset ($_POST["kukat"])) {
			echo "<h3>Valinnat talletettu</h3>";		
		} else {
			echo "<h3>Muista tallettaa valintasi!</h3>";
		} ?>	
		
	</div>

</div>


<div id="helpclosed">
	<p>?</p>
	<div id="helpwindow">
		<p>On normaalia, että kasvit tuottavat välillä mutanttihedelmiä ja kukkia. Mikäli oireet ovat kuitenkin kroonisia tai koskevat useampaa kasvia, merkitse se listaan</p>
	</div>
</div>

</div>

</body>

</html>
