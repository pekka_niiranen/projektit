<!DOCTYPE html>

<?php
	session_start();
?>

<html>

<?php include("head.php"); ?>

<header>
</header>

<body >

<!--Sivuvalikko -->

<div id="leftmenu">
	<div class="menubutton" id="passibutton"><p><a href="ravinnetutka2.php">Valonsaanti</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="Kasvualusta.php">Kasvualusta</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="Lehdet.php">Lehdet</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="Kasvu.php">Kasvu</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="runko.php">Runko</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="hedelmät-kukat.php">Hedelmät</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="lämpötila.php">Lämpötila</a></p></div>
	<div class="menubutton" id="actibutton"><p><a href="ratkaise.php">Ratkaise</a></p></div>
</div>

<!--Keskinäkymä-->

<div id="center-view">

	<div id="back"><a href="lämpötila.php">Takaisin<a/></div>
	<div id="next"><a href="ravinnetutka2.php">Aloita alusta<a/></div>

	<div id="options">
 
		<br>

		
		<?php 

		include("logicR.php");
		include("messages.php");

		/*echo bugs();
		echo multiblock(); 
		echo overfert();L
		echo underfert(); */
		
		$bug = bugs();
		$mb = multiblock();
		$ovf = overfert();
		$udf = underfert();
		/*echo $bug, $mb, $ovf, $udf, $ldep;*/

		if ($bug==="bugs") {
			echo $bug_message;
		} else if ($mb ==="multiblock") {
			echo $multiblock_message;
		} else if ($ovf==="overfert") {
			echo $overfert_message;
		} else if ($udf==="N") {
			echo $N_message;
		} else if ($udf==="P") {
			echo $P_Message;
		} else if ($udf==="K") {
			echo $K_Message;
		} else if ($udf ==="Micro") {
			echo $Micro_message;
		} else {
			echo "Valitettavasti ongelmaa ei voitu diagnosoida. Tietoja ei ole riittävästi. Muistithan varmasti tallentaa valintasi?";
		} 

		?>

		<?php 
			include("logicL.php");

			$Lid = ldepr();
			if ($Lid==="LD") {
				echo $lightdep_message;
			} else {}
		?>



		
	</div>

</div>




</div>
	<div id="helpclosed">
	<p>?</p>
	<div id="helpwindow">
		<p>Eikö ratkaisua löytynyt? Ravinnetutka ei ole vielä täydellinen, eikä mielellään diagnosoi liian vähäisiä ongelmia. Näin vältetään lisävahinkoja. Merkitsitkö varmasti kaikki oireet ylös? Valintojen tallentaminen unohtuu helposti. Voit palata alkuun, ja yrittää uudestaan</p>
	</div>
</div>
</body>

</html>
