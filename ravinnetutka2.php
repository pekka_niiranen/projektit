<!DOCTYPE html>

<?php 
	session_start();
	$fullsun = "unchecked";
	$halfshade = "unchecked";
?>

<html>

<?php include("head.php"); ?>

<header>
</header>

<body >

<!--Sivuvalikko -->

<div id="leftmenu">
	<div class="menubutton" id="actibutton"><p><a href="ravinnetutka2.php">Valonsaanti</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="Kasvualusta.php">Kasvualusta</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="Lehdet.php">Lehdet</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="Kasvu.php">Kasvu</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="runko.php">Runko</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="hedelmät-kukat.php">Hedelmät</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="lämpötila.php">Lämpötila</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="ratkaise.php">Ratkaise</a></p></div>
</div>

<!--Keskinäkymä-->

<div id="center-view">

	<div id="next"><a href="Kasvualusta.php">Seuraava</a></div>

	<div id="options">
		<h3>Valitse kasvisi valonsaanti</h3>
		
		<!--<form name="valo" action="" method="POST" target="">
			<input type="radio" name="Light"  value="full_sun" <?php echo $fullsun; ?>/>Täysi aurinko <br>
			<input type="radio" name="Light"  value="half_shade"/>Puolivarjo <br>
			<input type="radio" name="Light"  value="shade"/>Varjo <br>
			<input type="radio" name="Light"  value="lamp"/>Keinovalo <br>
			<input type="submit" name="submit" value="submit"/>
		</form> -->

		<!-- PHP script valintojen tallettamista varten -->

		<?php 
			if (isset($_POST["Light"])) {
				$_SESSION["Light"] = $_POST["Light"];
						
			} else {}
			
		?>

		<form name="valo" action="" method="POST" target="">
			<input type="radio" name="Light"  value="full_sun" <?php if($_SESSION["Light"]=="full_sun") {echo "checked";} else {} ?>/>Täysi aurinko <br>
			<input type="radio" name="Light"  value="half_shade"<?php if($_SESSION["Light"]=="half_shade") {echo "checked";} else {} ?>/>Puolivarjo <br>
			<input type="radio" name="Light"  value="shade"<?php if($_SESSION["Light"]=="shade") {echo "checked";} else {} ?>/>Varjo <br>
			<input type="radio" name="Light"  value="lamp"<?php if($_SESSION["Light"]=="lamp") {echo "checked";} else {} ?>/>Keinovalo <br>
			<input type="submit" name="submit" value="Tallenna"/>
		</form>

		<?php if (isset ($_POST["Light"])) {
			echo "<h3>Valinnat talletettu</h3>";		
		} else {
			echo "<h3>Muista tallettaa valintasi!</h3>";		
		}?>

		
		

		
		
	</div>

	

</div>

<div id="helpclosed">
	<p>?</p>
	<div id="helpwindow">
		<p>Yleensä kasvit tarvitsevat paljon valoa kasvaakseen hyvin. Merkitse kasvisi valonsaanti listaan, mikäli tiedät sen</p>
	</div>
</div>



</div>

</body>

</html>
