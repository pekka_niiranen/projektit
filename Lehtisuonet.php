<!DOCTYPE html>

<?php 
	session_start();

?>

<html>

<?php include("head.php"); ?>

<header>
</header>

<body >

<!--Sivuvalikko -->

<div id="leftmenu">
	<div class="menubutton" id="passibutton"><p><a href="ravinnetutka2.php">Valonsaanti</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="Kasvualusta.php">Kasvualusta</a></p></div>
	<div class="menubutton" id="actibutton"><p><a href="Lehdet.php">Lehdet</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="Kasvu.php">Kasvu</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="runko.php">Runko</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="hedelmät-kukat.php">Hedelmät</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="lämpötila.php">Lämpötila</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="ratkaise.php">Ratkaise</a></p></div>
</div>

<!--Keskinäkymä-->

<div id="center-view">

	<div id="back"><a href="Lehdet.php">Takaisin</a></div>
	<div id="next"><a href="muoto.php">Seuraava</a></div>

	<div id="options">

		<!-- PHP script valintojen tallettamista varten -->

		<?php 
			if (isset($_POST["veins"])) {
				$_SESSION["veins"] = $_POST["veins"];			
			} else {}

		?>		


		<div id="subform" class="passiv"><p><a href="Lehdet.php">Väri</a></p></div>
		<div id="subform" class="activ"><p><a href="Lehtisuonet.php">Lehtisuonet</a></p></div>
		<div id="subform" class="passiv"><p><a href="muoto.php">Muoto</a></p></div>
		<div id="subform" class="passiv"><p><a href="alapinta.php">Alapinta</a></p></div>
		<br>
		<form name="Lsuonet" action="" method="POST" target="">
			<input type="checkbox" name="veins[]" value="yel" <?php if(@in_array ("yel", $_SESSION["veins"])) {echo "checked";} else {} ?>/>Keltaiset<br>
			<input type="checkbox" name="veins[]" value="gre" <?php if(@in_array ("gre", $_SESSION["veins"])) {echo "checked";} else {} ?>/>Vihreät<br>
			<input type="checkbox" name="veins[]" value="dark" <?php if(@in_array ("dark", $_SESSION["veins"])) {echo "checked";} else {} ?>/>Tummat<br>
			<input type="submit" name="submit" value="Tallenna"/>

		</form>

		<?php if (isset ($_POST["veins"])) {
			echo "<h3>Valinnat talletettu</h3>";		
		} else {
			echo "<h3>Muista tallettaa valintasi!</h3>";
		} ?>
		
		
	</div>

</div>


<div id="helpclosed">
	<p>?</p>
	<div id="helpwindow">
		<p>Tarkista kasviesi lehtisuonten väri. Yleensä jotkut sairaudet alkavat lehtisuonista. Muista kuitenkin, että pienet poikkeamat ovat normaaleja</p>
	</div>
</div>

</div>

</body>

</html>
