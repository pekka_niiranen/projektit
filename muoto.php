<!DOCTYPE html>

<?php 
	session_start();
	
?>

<html>

<?php include("head.php"); ?>

<header>
</header>

<body >

<!--Sivuvalikko -->

<div id="leftmenu">
	<div class="menubutton" id="passibutton"><p><a href="ravinnetutka2.php">Valonsaanti</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="Kasvualusta.php">Kasvualusta</a></p></div>
	<div class="menubutton" id="actibutton"><p><a href="Lehdet.php">Lehdet</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="Kasvu.php">Kasvu</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="runko.php">Runko</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="hedelmät-kukat.php">Hedelmät</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="lämpötila.php">Lämpötila</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="ratkaise.php">Ratkaise</a></p></div>
</div>

<!--Keskinäkymä-->

<div id="center-view">

	<div id="back"><a href="Lehtisuonet.php">Takaisin</a></div>
	<div id="next"><a href="alapinta.php">Seuraava</a></div>

	<div id="options">

		<!-- PHP script valintojen tallettamista varten -->

		<?php 
			if (isset($_POST["shape"])) {
				$_SESSION["shape"] = $_POST["shape"];			
			} else {}

		?>

		<div id="subform" class="passiv"><p><a href="Lehdet.php">Väri</a></p></div>
		<div id="subform" class="passiv"><p><a href="Lehtisuonet.php">Lehtisuonet</a></p></div>
		<div id="subform" class="activ"><p><a href="muoto.php">Muoto</a></p></div>
		<div id="subform" class="passiv"><p><a href="alapinta.php">Alapinta</a></p></div>
		<br>
		<form name="Muoto" action="" method="POST" target="">
			<input type="checkbox" name="shape[]" value="up" <?php if(@in_array ("up", $_SESSION["shape"])) {echo "checked";} else {} ?>/>Käpristynyt ylöspäin<br>
			<input type="checkbox" name="shape[]" value="down" <?php if(@in_array ("down", $_SESSION["shape"])) {echo "checked";} else {} ?>/>Alaspäin vääntynyt "koukku"<br>
			<input type="submit" name="submit" value="Tallenna"/>

		</form>

		<?php if (isset ($_POST["shape"])) {
			echo "<h3>Valinnat talletettu</h3>";		
		} else {
			echo "<h3>Muista tallettaa valintasi!</h3>";
		} ?>
		
		
	</div>

</div>


<div id="helpclosed">
	<p>?</p>
	<div id="helpwindow">
		<p>Lehdet eivät ole koskaan täydellisen suoria, mutta mikäli kasviesi lehdet ovat vääntyneet selkeästi "kupin" muotoisiksi, alas tai ylöspäin, merkkaa tulos listaan.</p>
	</div>
</div>

</div>

</body>

</html>
