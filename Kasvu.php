<!DOCTYPE html>

<?php 
	session_start();
?>

<html>

<?php include("head.php"); ?>

<header>
</header>

<body >

<!--Sivuvalikko -->

<div id="leftmenu">
	<div class="menubutton" id="passibutton"><p><a href="ravinnetutka2.php">Valonsaanti</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="Kasvualusta.php">Kasvualusta</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="Lehdet.php">Lehdet</a></p></div>
	<div class="menubutton" id="actibutton"><p><a href="Kasvu.php">Kasvu</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="runko.php">Runko</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="hedelmät-kukat.php">Hedelmät</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="lämpötila.php">Lämpötila</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="ratkaise.php">Ratkaise</a></p></div>
</div>

<!--Keskinäkymä-->

<div id="center-view">

	<div id="back"><a href="alapinta.php">Takaisin</a></div>
	<div id="next"><a href="runko.php">Seuraava</a></div>

	<div id="options">

		<!-- PHP script valintojen tallettamista varten -->

		<?php 
			if (isset($_POST["grow"])) {
				$_SESSION["grow"] = $_POST["grow"];	
			} else {}
		?>
		<h3>Miten luonnehtisit kasvisi kasvua?</h3>
		<br>
		<form name="kasvu" action="" method="POST" target="">
			<input type="radio" name="grow" value="norm" checked <?php if($_SESSION["grow"]=="norm") {echo "checked";} else {} ?> />Normaalia <br>
			<input type="radio" name="grow" value="fast" <?php if($_SESSION["grow"]=="fast") {echo "checked";} else {} ?>/>Nopeaa<br>
			<input type="radio" name="grow" value="slow" <?php if($_SESSION["grow"]=="slow") {echo "checked";} else {} ?>/>Hidasta<br>
			<input type="submit" name="submit" value="Tallenna"/>

		</form>

		<?php if (isset ($_POST["grow"])) {
			echo "<h3>Valinnat talletettu</h3>";		
		} else {
			echo "<h3>Muista tallettaa valintasi!</h3>";
		}?>
		
		
	</div>

</div>


<div id="helpclosed">
	<p>?</p>
	<div id="helpwindow">
		<p>Kasvuvauhti on yleensä yhteydessä kasvien sairauksiin, ja auttaa diagnosoinnissa.</p>
	</div>
</div>

</div>

</body>

</html>
