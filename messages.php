<?php


	$N_message = "Kasvisi kärsivät todennäköisesti typenpuutteesta.";
	$P_message = "Kasvisi kärsivät todennäköisesti fosforinpuutteesta.";
	$K_message = "Kasvisi kärsivät todennäköisesti kaliuminpuutteesta.";
	$Micro_message = "Kasvisi kärsivät todennäköisesti hivenaineiden puutteesta.";
	$bug_message = "Kasvillasi on tuholaistartunta. Voit korjata tartunnan pyristidiiniä sisältävällä tuholaismyrkyllä";
	$overfert_message = "Kasvisi kärsivät todennäköisesti ylilannoituksesta. Pidättäydy lannoittamasta kasvejasi kunnes oireet loppuvat";
	$multiblock_message = "Kasvisi kärsivät ravinnetukoksesta. Liian suuri lannoitteiden määrä on järkyttänyt maaperän tasapainoa ja todennäköisesti sekoittanut kasvien aineenvaihdunnan. Huuhtele kasvin multa suurella määrällä puhdasta vettä ja pidättäydy lannoittamasta, kunnes oireet loppuvat";
	$lightdep_message = "Valoa voi olla myös liian vähän. Aseta kasvi joko aurinkoisempaan paikkaan, tai tarjoa sille lisävaloa.";
	

?>
