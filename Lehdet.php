<!DOCTYPE html>

<?php 
	session_start();

?>

<html>

<?php include("head.php"); ?>

<header>
</header>

<body >

<!--Sivuvalikko -->

<div id="leftmenu">
	<div class="menubutton" id="passibutton"><p><a href="ravinnetutka2.php">Valonsaanti</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="Kasvualusta.php">Kasvualusta</a></p></div>
	<div class="menubutton" id="actibutton"><p><a href="Lehdet.php">Lehdet</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="Kasvu.php">Kasvu</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="runko.php">Runko</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="hedelmät-kukat.php">Hedelmät</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="lämpötila.php">Lämpötila</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="ratkaise.php">Ratkaise</a></p></div>
</div>

<!--Keskinäkymä-->

<div id="center-view">
	<div id="back"><a href="Kasvualusta.php">Takaisin</a></div>
	<div id="next"><a href="Lehtisuonet.php">Seuraava</a></div>


	<div id="options">

		<!-- PHP script valintojen tallettamista varten -->

		<?php 
			if (isset($_POST["Leaf"])) {
				$_SESSION["Leaf"] = $_POST["Leaf"];			
			} else {}

		?>		

		<div id="subform" class="activ" ><p><a href="Lehdet.php">Väri</a></p></div>
		<div id="subform" class="passiv"><p><a href="Lehtisuonet.php">Lehtisuonet</a></p></div>
		<div id="subform" class="passiv"><p><a href="muoto.php">Muoto</a></p></div>
		<div id="subform" class="passiv"><p><a href="alapinta.php">Alapinta</a></p></div>
		<br>
		<br>
		<h3>Minkälaisia värejä kasviesi lehdissä ilmenee?</h3>
		<form name="Lehdet" action="" method="POST" target="">
			<input type="checkbox" name="Leaf[]" value="yel" <?php if(@in_array ("yel", $_SESSION["Leaf"])) {echo "checked";} else {} ?>/>Keltainen<br>
			<input type="checkbox" name="Leaf[]" value="lgreen" <?php if(@in_array ("lgreen", $_SESSION["Leaf"])) {echo "checked";} else {} ?>/>Vaaleanvihreä<br>
			<input type="checkbox" name="Leaf[]" value="dgreen" <?php if(@in_array ("dgreen", $_SESSION["Leaf"])) {echo "checked";} else {} ?>/>Epäluonnollisen tummanvihreä<br>
			<input type="checkbox" name="Leaf[]" value="purp" <?php if(@in_array ("purp", $_SESSION["Leaf"])) {echo "checked";} else {} ?>/>Violetti<br>
			<input type="checkbox" name="Leaf[]" value="white" <?php if(@in_array ("white", $_SESSION["Leaf"])) {echo "checked";} else {} ?>/>Valkoinen<br>
			<input type="checkbox" name="Leaf[]" value="burn" <?php if(@in_array ("burn", $_SESSION["Leaf"])) {echo "checked";} else {} ?>/>Palanut<br>
			<input type="checkbox" name="Leaf[]" value="spot" <?php if(@in_array ("spot", $_SESSION["Leaf"])) {echo "checked";} else {} ?>/>Täplikäs<br>
			<br>
			<input type="checkbox" name="Leaf[]" value ="drop" />Lehdet tippuvat<br>
			<input type="submit" name="submit" value="Tallenna"/>

		</form>
		
		

		
			<!--<?php foreach ($_SESSION["Leaf"] as $arvo) {
			echo $arvo . " "; 
		}  ?> -->

			<?php if (isset ($_POST["Leaf"])) {
			echo "<h3>Valinnat talletettu</h3>";		
		} else {
			echo "<h3>Muista tallettaa valintasi!</h3>";
		}?>


	</div>
	

</div>


<div id="helpclosed">
	<p>?</p>
	<div id="helpwindow">
		<p>Lehdet ovat se paikka, josta kasvien sairaudet yleensä ensin alkavat. Pari keltaista lehteä silloin tällöin on normaalia, mutta mikäli kellastuminen on nopeaa, on kyseessä ongelma</p>
	</div>
</div>

</div>

</body>

</html>
