<!DOCTYPE html>

<?php
	session_start();

?>

<html>

<?php include("head.php"); ?>

<header>
</header>

<body >

<!--Sivuvalikko -->

<div id="leftmenu">
	<div class="menubutton" id="passibutton"><p><a href="ravinnetutka2.php">Valonsaanti</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="Kasvualusta.php">Kasvualusta</a></p></div>
	<div class="menubutton" id="actibutton"><p><a href="Lehdet.php">Lehdet</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="Kasvu.php">Kasvu</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="runko.php">Runko</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="hedelmät-kukat.php">Hedelmät</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="lämpötila.php">Lämpötila</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="ratkaise.php">Ratkaise</a></p></div>
</div>

<!--Keskinäkymä-->

<div id="center-view">

	<div id="back"><a href="muoto.php">Takaisin</a></div>
	<div id="next"><a href="Kasvu.php">Seuraava</a></div>

	<div id="options">
		<!-- PHP script valintojen tallettamista varten -->
		<?php 
			if (isset($_POST["surf"])) {
				$_SESSION["surf"] = $_POST["surf"];	
			} else {}
		?>

		<!-- Alivalikko -->

		<div id="subform" class="passiv"><p><a href="Lehdet.php">Väri</a></p></div>
		<div id="subform" class="passiv"><p><a href="Lehtisuonet.php">Lehtisuonet</a></p></div>
		<div id="subform" class="passiv"><p><a href="muoto.php">Muoto</a></p></div>
		<div id="subform" class="activ"><p><a href="alapinta.php">Alapinta</a></p></div>
		<br>
		
		<!-- Valinnat -->

		<form name="alapinta" action="" method="POST" target="">
			<input type="checkbox" name="surf[]" value="hole" <?php if(@in_array ("hole", $_SESSION["surf"])) {echo "checked";} else {} ?>/>Reikäinen<br>
			<input type="checkbox" name="surf[]" value="bspot" <?php if(@in_array ("bspot", $_SESSION["surf"])) {echo "checked";} else {} ?>/>Mustia pisteitä<br>
			<input type="checkbox" name="surf[]" value="web" <?php if(@in_array ("web", $_SESSION["surf"])) {echo "checked";} else {} ?>/>Seittiä<br>
			<input type="submit" name="submit" value="Tallenna" <?php if(@in_array ("submit", $_SESSION["surf"])) {echo "checked";} else {} ?>/>

		</form>

		<!-- Notifier scripti  -->
		
		<?php if (isset ($_POST["surf"])) {
			echo "<h3>Valinnat talletettu</h3>";		
		} else {
			echo "<h3>Muista tallettaa valintasi!</h3>";
		} ?>




		
	</div>

</div>

<!-- Help-valikko -->

<div id="helpclosed">
	<p>?</p>
	<div id="helpwindow">
		<p>Varsinkin ulkona kasvit saattavat saada tuholaistartuntoja. Yleensä nämä ötökät kertyvät kasvin lehtien alapinnoille</p>
	</div>
</div>

</div>

</body>

</html>
