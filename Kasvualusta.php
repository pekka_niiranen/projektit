<!DOCTYPE html>

<?php 
	session_start();
?>

<html>

<?php include("head.php"); ?>

<header>
</header>

<body >

<!--Sivuvalikko -->

<div id="leftmenu">
	<div class="menubutton" id="passibutton"><p><a href="ravinnetutka2.php">Valonsaanti</a></p></div>
	<div class="menubutton" id="actibutton"><p><a href="Kasvualusta.php">Kasvualusta</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="Lehdet.php">Lehdet</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="Kasvu.php">Kasvu</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="runko.php">Runko</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="hedelmät-kukat.php">Hedelmät</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="lämpötila.php">Lämpötila</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="ratkaise.php">Ratkaise</a></p></div>
</div>

<!--Keskinäkymä-->

<div id="center-view">

	<div id="back"><a href="ravinnetutka2.php">Takaisin</a></div>
	<div id="next"><a href="Lehdet.php">Seuraava</a></div>


	<div id="options">

		<!-- PHP script valintojen tallettamista varten -->

		<?php 
		if (isset($_POST["medium"])) {
			$_SESSION["medium"] = $_POST["medium"];			
		} else {}

		?>
		<h3>Valitse kasvisi kasvatusalusta</h3>
	
		<form name="kasvualusta" action="" method="POST" target="">
			<input type="radio" name="medium" value="dirt"<?php if($_SESSION["medium"]=="dirt") {echo "checked";} else {} ?>/>Multa <br>
			<input type="radio" name="medium" value="hydro"<?php if($_SESSION["medium"]=="hydro") {echo "checked";} else {} ?>/>Vesiviljely <br>
			<input type="submit" name="submit" value="Tallenna">

		</form>
		
		<?php if (isset ($_POST["medium"])) {
			echo "<h3>Valinnat talletettu</h3>";		
		} else {
			echo "<h3>Muista tallettaa valintasi!</h3>";
		}?>

		
	</div>

</div>


<div id="helpclosed">
	<p>?</p>
	<div id="helpwindow">
		<p>Kasvualustalla on merkitystä ravinteiden imeytymisen kannalta. Mikäli kasvisi kasvaa mullassa, se ei ole vesiviljelty</p>
	</div>
</div>

</div>

</body>

</html>
