<!DOCTYPE html>

<?php 
	session_start();

?>

<html>

<?php include("head.php"); ?>

<header>
</header>

<body>

<div id="start-center">

	<h1>Tervetuloa Ravinnetutkaan</h1>
	<h3>Ravinnetutkalla voit diagnosoida ruoka- tai huonekasviesi huonon kasvun, tai sairauksien syitä</h3>
	<br><br>
	<h3>Kyselyssä ei ole pakollisia kenttiä, mutta mitä useamman kohdan täytät, sitä todennäköisemmin saat ratkaisun. Mikäli tarvitset apua, paina ruudun oikeassa yläkulmassa sijaitsevaa kysymysmerkkiä</h3>

	<div id="startbutton"><h3><a href="ravinnetutka2.php">Aloita tästä</a></h3></div>

</div>

</body>

</html>
