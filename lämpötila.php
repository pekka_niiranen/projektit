<!DOCTYPE html>

<?php 
	session_start();
?>

<html>

<?php include("head.php"); ?>

<header>
</header>

<body >

<!--Sivuvalikko -->

<div id="leftmenu">
	<div class="menubutton" id="passibutton"><p><a href="ravinnetutka2.php">Valonsaanti</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="Kasvualusta.php">Kasvualusta</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="Lehdet.php">Lehdet</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="Kasvu.php">Kasvu</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="runko.php">Runko</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="hedelmät-kukat.php">Hedelmät</a></p></div>
	<div class="menubutton" id="actibutton"><p><a href="lämpötila.php">Lämpötila</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="ratkaise.php">Ratkaise</a></p></div>
</div>

<!--Keskinäkymä-->

<div id="center-view">

	<div id="back"><a href="hedelmät-kukat.php">Takaisin</a></div>
	<div id="next"><a href="ratkaise.php">Seuraava</a></div>

	<div id="options">

		<!-- PHP script valintojen tallettamista varten -->
		
		<?php 
			if (isset($_POST["temp"])) {
				$_SESSION["temp"] = $_POST["temp"];	
			} else {}
		?>
		
		<p>Mikäli tiedät ulko- tai sisälämpötilan, aseta se tähän</p>
		<form name="temp" action="" method="POST" target="">
			Lämpötila
			<input type="number" name="temp" min="-30" max="40"/>
			<input type="submit" name="submit" value="Tallenna"/>
		</form>

		<?php if (isset ($_POST["temp"])) {
			echo "<h3>Valinnat talletettu</h3>";		
		} else {
			echo "<h3>Muista tallettaa valintasi!</h3>";
		}?>
		
	</div>

</div>


<div id="helpclosed">
	<p>?</p>
	<div id="helpwindow">
		<p>Liian matala tai korkea lämpötila voivat sekoittaa kasvien aineenvaihdunnan. Mikäli tiedät lämpötilan, merkkaa se tähän.</p>
	</div>
</div>

</div>

</body>

</html>
