<!DOCTYPE html>

<?php 
	session_start();
?>

<html>

<?php include("head.php"); ?>

<header>
</header>

<body >

<!--Sivuvalikko -->

<div id="leftmenu">
	<div class="menubutton" id="passibutton"><p><a href="ravinnetutka2.php">Valonsaanti</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="Kasvualusta.php">Kasvualusta</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="Lehdet.php">Lehdet</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="Kasvu.php">Kasvu</a></p></div>
	<div class="menubutton" id="actibutton"><p><a href="runko.php">Runko</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="hedelmät-kukat.php">Hedelmät</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="lämpötila.php">Lämpötila</a></p></div>
	<div class="menubutton" id="passibutton"><p><a href="ratkaise.php">Ratkaise</a></p></div>
</div>

<!--Keskinäkymä-->

<div id="center-view">

	<div id="back"><a href="Kasvu.php">Takaisin</a></div>
	<div id="next"><a href="hedelmät-kukat.php">Seuraava</a></div>

	<div id="options">

		<!-- PHP script valintojen tallettamista varten -->

		<?php 
			if (isset($_POST["stem"])) {
				$_SESSION["stem"] = $_POST["stem"];	
			} else {}
		?>

		<h3>Millainen on kasvisi rungon rakenne?</h3>
		<form name="runko" action="" method="POST" target="">
			<input type="radio" name="stem" value="norm" checked <?php if($_SESSION["stem"]=="norm") {echo "checked";} else {} ?>/>Normaali <br>
			<input type="radio" name="stem" value="weak" <?php if($_SESSION["stem"]=="weak") {echo "checked";} else {} ?>/>Heikko<br>
			<input type="submit" name="submit" value="Tallenna"/>

		</form>

		<?php if (isset ($_POST["stem"])) {
			echo "<h3>Valinnat talletettu</h3>";		
		} else {
			echo "<h3>Muista tallettaa valintasi!</h3>";		
		}?>
		
		
	</div>

</div>


<div id="helpclosed">
	<p>?</p>
	<div id="helpwindow">
		<p>Jotkut kasvit kehittävät luontaisesti ohuet ja heiveröiset rungot. Mikäli kasviesi uusi kasvusto on kuitenkin merkittävästi aiempaa kasvua heikompaa, merkkaa tulos listaan.</p>
	</div>
</div>

</div>

</body>

</html>
